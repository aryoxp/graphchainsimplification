package ap.mobile.graphchainsimplification;

import android.content.res.Resources;

/**
 * Created by Aryo on 3/20/2017.
 */

public class Helper {

    public static double calculateDistance(PointTransport a, PointTransport b) {
        Double distance = Math.sqrt(Math.pow((a.lat() - b.lat()), 2) + Math.pow((a.lng() - b.lng()), 2));
        return distance;
    }

    public static double calculateDistance(PointTransport a, Double lat, Double lng) {
        Double distance = Math.sqrt(Math.pow((a.lat() - lat), 2) + Math.pow((a.lng() - lng), 2));
        return distance;
    }

    public static int pxToDp(int px) {
        return (int) (px / Resources.getSystem().getDisplayMetrics().density);
    }

    public static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

}
