package ap.mobile.graphchainsimplification;

import android.content.Context;
import android.preference.PreferenceManager;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Aryo on 3/28/2018.
 */

public class CDM {
    
    public static String getDummyJson(Context context) {

        InputStream inputStream = context.getResources().openRawResource(R.raw.points);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        byte buf[] = new byte[1024];
        int len;
        try {
            while ((len = inputStream.read(buf)) != -1) {
                outputStream.write(buf, 0, len);
            }
            outputStream.close();
            inputStream.close();
        } catch (IOException e) {

        }
        return outputStream.toString();

    }

    public static String getApiBaseUrl(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString("basepath", "http://175.45.187.243/routing");
    }

    public static Double getStandardCost() { return 4000D; }

    public static Double oneMeterInDegree() { return 0.00000898448D; }
}
