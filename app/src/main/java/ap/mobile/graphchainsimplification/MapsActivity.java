package ap.mobile.graphchainsimplification;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Point;
import android.location.Location;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnSuccessListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.AbstractCollection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleMap.OnMapLongClickListener, GoogleMap.OnMapLoadedCallback {

    private GoogleMap mMap;
    private FusedLocationProviderClient mFusedLocationClient;
    private ActionBar mActionBar;
    private String[] mPermissions = {
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
    };
    private int mLocationRequestCode = 77;
    private Marker mUserLocationMarker;
    private ArrayList<Polyline> pathPolylines = new ArrayList<>();
    private ArrayList<Polyline> pathPolylinesSimplified = new ArrayList<>();
    private LatLngBounds mBounds;
    private ArrayList<Polyline> subChainPolylines = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PreferenceManager.setDefaultValues(this, R.xml.pref_service, false);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        this.mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        this.mActionBar = this.getSupportActionBar();
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.mMap = googleMap;
        this.mMap.setOnMapLongClickListener(this);

        String pointsJsonString = CDM.getDummyJson(this);
        ArrayList<PointTransport> points = new ArrayList<>();
        ArrayList<Marker> pointsMarkers = new ArrayList<>();
        try {
            JSONArray pointsJsonArray = new JSONArray(pointsJsonString);
            for(Marker m: pointsMarkers) m.remove();
            pointsMarkers.clear();
            for(int i = 0; i < pointsJsonArray.length(); i++) {
                JSONObject pointJsonObject = (JSONObject) pointsJsonArray.get(i);
                // {"id":"5","lat":"-7.967430","lng":"112.638549","l":"1","st":"0","n":"CKL","d":"O","c":"#FF0000","s":"5","a":"7","i":"6"}
                String id = pointJsonObject.getString("id");
                double lat = Double.valueOf(pointJsonObject.getString("lat"));
                double lng = Double.valueOf(pointJsonObject.getString("lng"));
                int lineId = Integer.valueOf(pointJsonObject.getString("l"));
                Boolean isStop = Boolean.valueOf(pointJsonObject.getString("st"));
                String lineName = pointJsonObject.getString("n");
                String direction = pointJsonObject.getString("d");
                String color = pointJsonObject.getString("c");
                int sequence = Integer.valueOf(pointJsonObject.getString("s"));
                String adjacentPointId = pointJsonObject.getString("a");
                String interchangeId = pointJsonObject.getString("i");

                PointTransport p = new PointTransport(id, lat, lng, isStop, lineId, lineName, direction, color, sequence, adjacentPointId, interchangeId);
                points.add(p);

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        Set<PointTransport> graphPoints = Graph.build(points);

        /*
        for(Polyline polyline: this.pathPolylines)
            polyline.remove();
        this.pathPolylines.clear();

        for(PointTransport point : graphPoints) {
            Log.d("GCS", point.getId() + " degree: " + point.getDegree());
            MarkerOptions markerOptions = new MarkerOptions()
                    .position(new LatLng(point.lat(), point.lng()))
                    .title(point.getId() + ":" + point.getLineName() + " d:" + point.getDegree())
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
                    //.snippet("Lat: " + point.lat + " Lng: " + lng);
            pointsMarkers.add(this.mMap.addMarker(markerOptions));
            Map<PointTransport, PointTransport.TransportCost> nextPoints = point.getAdjacentTransportPoints();
            for(Map.Entry<PointTransport, PointTransport.TransportCost> entry: nextPoints.entrySet()) {
                PointTransport nextPoint = entry.getKey();
                PointTransport.TransportCost nextCost = entry.getValue();
                PolylineOptions polylineOptions = new PolylineOptions()
                        .add(point.getLatLng())
                        .add(nextPoint.getLatLng())
                        .color(Color.RED)
                        .width(9f);
                Polyline p = this.mMap.addPolyline(polylineOptions);
                this.pathPolylines.add(p);
            }
        }
        */



        PointTransport p = Graph.hasChain(graphPoints);


        Queue<PointTransport> chainCandidates = new LinkedList<>();

        // Finding chain candidates...
        for(PointTransport pointTransport : graphPoints) {
            if(pointTransport.getDegree() == 2) {
                Set<PointTransport> prevSet = pointTransport.getPreviousTransportPoints().keySet();
                if(prevSet.iterator().next().getDegree() != 2)
                    chainCandidates.add(pointTransport);
            }
        }

        // creating subChains...
        while(chainCandidates.peek() != null) {
            PointTransport head, tail;
            PointTransport observedPoint = chainCandidates.remove();

            head = observedPoint.previousTransportPoints.keySet().iterator().next();

            // detach from head
            PointTransport.TransportCost costToChain
                    = head.getAdjacentTransportPoints().get(observedPoint);
            head.getAdjacentTransportPoints().remove(observedPoint);

            LinkedHashMap<PointTransport, PointTransport.TransportCost> subChain
                    = new LinkedHashMap<>();

            // add current observed point as the beginning of the subChain
            subChain.put(observedPoint, costToChain);

            do {

                // remove detached observed point from graph
                graphPoints.remove(observedPoint);

                // get next point in chain
                Map.Entry<PointTransport, PointTransport.TransportCost> next = follow(observedPoint);
                if(next.getKey().getDegree() != 2) { // found tail
                    tail = next.getKey();
                    subChain.put(tail, next.getValue());
                    break;
                }
                subChain.put(next.getKey(), next.getValue());
                observedPoint = next.getKey();

            } while(true);

            PointTransport.TransportCost subCost = new PointTransport.TransportCost();
            for(PointTransport.TransportCost entryCost : subChain.values()) {
                subCost.distance += entryCost.distance;
                subCost.price += entryCost.price;
            }

            head.addDestination(tail, subCost);
            head.addSubChain(tail, subChain);

        }

        for(Polyline polyline: this.pathPolylinesSimplified)
            polyline.remove();
        this.pathPolylinesSimplified.clear();

        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for(PointTransport point : graphPoints) {

            builder.include(point.getLatLng());

            Log.d("GCS", point.getId() + " degree: " + point.getDegree());
            MarkerOptions markerOptions = new MarkerOptions()
                    .position(new LatLng(point.lat(), point.lng()))
                    .title(point.getId() + ":" + point.getLineName() + " d:" + point.getDegree())
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
            //if(point.getDegree() == 2)
            if(chainCandidates.contains(point))
                markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE));
            //.snippet("Lat: " + point.lat + " Lng: " + lng);
            pointsMarkers.add(this.mMap.addMarker(markerOptions));

            Map<PointTransport, PointTransport.TransportCost> nextPoints = point.getAdjacentTransportPoints();

            for(Map.Entry<PointTransport, PointTransport.TransportCost> entry: nextPoints.entrySet()) {

                PointTransport nextPoint = entry.getKey();

                LinkedHashMap<PointTransport, PointTransport.TransportCost> subChain = point.getSubChain(nextPoint);
                if(subChain != null) {
                    PolylineOptions subChainPolylineOptions = new PolylineOptions()
                            .add(point.getLatLng())
                            .color(Color.RED)
                            .width(9f);
                    for(PointTransport subChainPoint : subChain.keySet()) {
                        subChainPolylineOptions
                                .add(subChainPoint.getLatLng());
                        builder.include(subChainPoint.getLatLng());
                    }
                    Polyline psc = this.mMap.addPolyline(subChainPolylineOptions);
                    this.subChainPolylines.add(psc);
                }

                PointTransport.TransportCost nextCost = entry.getValue();
                PolylineOptions polylineOptions = new PolylineOptions()
                        .add(point.getLatLng())
                        .add(nextPoint.getLatLng())
                        .color(Color.BLUE)
                        .width(9f);
                Polyline ps = this.mMap.addPolyline(polylineOptions);
                this.pathPolylinesSimplified.add(ps);
            }
        }

        this.mBounds = builder.build();
        this.mMap.setOnMapLoadedCallback(this);


        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, this.mPermissions, this.mLocationRequestCode);
            return;
        }
        //showUserLocation();
    }

    private Map.Entry<PointTransport, PointTransport.TransportCost> follow(PointTransport observedPoint) {
        // found beginning of chain, follow
        Map<PointTransport, PointTransport.TransportCost> adjacentTransportPoints = observedPoint.getAdjacentTransportPoints();
        Map.Entry<PointTransport, PointTransport.TransportCost> next = adjacentTransportPoints.entrySet().iterator().next();
        return next;
    }


    private void showUserLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        this.mFusedLocationClient.getLastLocation()
            .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location location) {
                    // Got last known location. In some rare situations this can be null.
                    if (location != null) {
                        // Logic to handle location object
                        LatLng userLocation = new LatLng(location.getLatitude(), location.getLongitude());
                        mUserLocationMarker = mMap.addMarker(new MarkerOptions().position(userLocation));
                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(userLocation, 15f));
                    }
                }
            });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == this.mLocationRequestCode) {
            if(grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                //this.showUserLocation();
            }
        }
    }

    @Override
    public void onMapLongClick(LatLng latLng) {

    }

    @Override
    public void onMapLoaded() {
        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(this.mBounds, Helper.dpToPx(30));
        this.mMap.moveCamera(cu);
    }
}
