package ap.mobile.graphchainsimplification;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by aryo on 28/03/18.
 */

public class Graph {

    public static Set<PointTransport> build(ArrayList<PointTransport> points) {

        Set<PointTransport> pointSets = new HashSet<>();

        for (PointTransport pointTransport : points) {
            String adjacentPointId = pointTransport.getAdjacentPointId();
            for(PointTransport nextPointTransport : points) {
                if(adjacentPointId != null && adjacentPointId.equals(nextPointTransport.getId())) {
                    Double distance = Helper.calculateDistance(pointTransport, nextPointTransport);
                    PointTransport.TransportCost cost = new PointTransport.TransportCost(0D, distance);
                    pointTransport.addDestination(nextPointTransport, cost);
                    nextPointTransport.addSource(pointTransport, cost);
                }
            }

            //if(pointTransport.getId().equals("944")) {
            //    Log.d("MPT", pointTransport.getAdjacentTransportPoints().size()+"");
            //}

            String[] nextInterchangePoints = pointTransport.getInterchanges();
            if(nextInterchangePoints != null) {
                //if(pointTransport.getId().equals("944"))
                //    Log.d("MPT", "nextInterchanges" + pointTransport.getInterchanges().length+"");
                for(String nextInterchangePoint: nextInterchangePoints) {
                    for(PointTransport nextPointInterchange : points) {
                        if (nextInterchangePoint != null && nextInterchangePoint.equals(nextPointInterchange.getId())) {

                            //if(pointTransport.getId().equals("944"))
                            //    Log.d("MPT", "Found next interchanges" + nextPointInterchange +"");

                            PointTransport.TransportCost cost = new PointTransport.TransportCost(CDM.getStandardCost(), 0D);
                            pointTransport.addDestination(nextPointInterchange, cost);
                            nextPointInterchange.addSource(pointTransport, cost);
                        }
                    }
                }
            }

            //if(pointTransport.getId().equals("944")) {
            //    Log.d("MPT", pointTransport.getAdjacentTransportPoints().size()+"");
            //}
            pointSets.add(pointTransport);
        }

        return pointSets;
    }

    public static PointTransport hasChain(Set<PointTransport> graphPoints) {
        for(PointTransport p : graphPoints) {
            if (p.getDegree() == 2) return p;
        }
        return null;
    }

}
